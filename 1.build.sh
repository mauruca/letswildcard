#!/bin/sh
# Read LICENSE file

reponame=""
dnsservicepath=""

ShowLogo() {
  cat logo
  echo -e "\n"
}

SintaxShow() {
  echo "./build.sh <reponame>"
  exit 1
}

if [ -z "$1" ]; then
  SintaxShow
fi

ShowLogo

container="$1/certgen"

tagversion="$(cat version)"

docker build --rm --no-cache -t $container:$tagversion .
docker images $container*