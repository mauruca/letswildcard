#!/bin/sh
# Read LICENSE file

reponame=$1

ShowLogo() {
  cat logo
  echo -e "\n"
}

SintaxShow() {
  echo "./2.push.sh <reponame>"
  exit 1
}

ShowLogo

if [ -z "$reponame" ]
  then
    SintaxShow
fi

tagversion="$(cat version)"
docker push $reponame/certgen:$tagversion