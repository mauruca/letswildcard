#!/bin/bash
# Read LICENSE file

# variables
CERTOBOT_DRY=""
CERTOBOT_TEST=""
CERTBOT_MODE=" --manual"
CERTBOT_OPTIONS=""
CERTBOT_CONFIG_PATH=""

CONTAINER_DETACH=" -d" # default is detached
CONTAINER_RUINNING="false" # container state
CONTAINER_WORK_PATH="/opt/certbot"
CONTAINER_CERT_PATH="$CONTAINER_WORK_PATH/cert"
CONTAINER_DNSSERVICE_SCRIPT_PATH="$CONTAINER_WORK_PATH/dnsservice"
CONTAINER_DNSSERVICE_PROVIDER_SCRIPT_PATH=""

HOST_WORK_PATH="$PWD"
HOST_CERT_PATH="" # user defined host cert path
CERT_PATH="/etc/letsencrypt" # default host cert path
HOST_SCRIPTS=0 # default is to use container scripts
HOST_DNSSERVICE_SCRIPT_PATH="$HOST_WORK_PATH/dnsservice"
HOST_DNSSERVICE_PROVIDER_SCRIPT_PATH=""

VERBOSE=""
UNPRIVILEGED=""
DNSPROVIDER=""
DNSPROVIDERPATH=""

delay="192h"
tagversion="$(cat version)"
GID=$(id -g $USER)
OPT="options: [-h|--help] [-v|--verbose] [-t|--test] [-dr|--dryrun]\n \
      \t [-hs|--hostscripts <scripts path>]\n \
      \t [-of|--onefilepem]\n \
      \t [-cp|--certpath <certdir path>] [-u|--unprivileged <UID:GID>]\n"

ONEFILEPEM=0 #default split files, public, private, chain PEM generation

# required
REPOSITORY=""
DOMAIN=""
EMAIL=""

ShowLogo() {
  cat logo
  echo -e "\n"
}

ShowHelp() {
  echo "Todo"
}

ShowSintax() {
  echo -e "usage: 3.run.sh <options> -r <repository> -dp <dnsprovidername> -d <domain> -m <email>"
  echo -e "$OPT"
  exit 1
}

ArgumentsParse() {
  case $key in
      -h|--help)
      ShowHelp
      exit 0
      ;;
      -v|--verbose)
      VERBOSE=" -v"
      CONTAINER_DETACH="" # no detach in verbose
      ;;
      -dr|--dryrun)
      CERTOBOT_DRY=" --dry-run"
      ;;
      -t|--test)
      CERTOBOT_TEST=" --test-cert"
      ;;
      -of|--onefilepem)
      ONEFILEPEM=1
      ;;      
      -cp|--certpath)
      if [ ${arg:0:1} == "-" ]; then
          echo "-d used without domain name argument!"
          exit 1
      fi
      HOST_CERT_PATH=$arg
      ;;
      -hs|--hostscripts)
      if [ ${arg:0:1} == "-" ]; then
          echo "-hs used without scripts path name argument!"
          exit 1
      fi   
      HOST_SCRIPTS=1
      HOST_DNSSERVICE_PROVIDER_SCRIPT_PATH=$arg
      ;;
      -u|--unprivileged)
      if [ ${arg:0:1} == "-" ]; then
          echo "-u used without UID:GID argument!"
          exit 1
      fi
      UNPRIVILEGED=$arg
      ;;
      -dp|--dnsprovider)
      if [ ${arg:0:1} == "-" ]; then
          echo "-dp used without provider name argument!"
          exit 1
      fi
      DNSPROVIDER=$arg
      ;;      
      -r|--repository)
      if [ ${arg:0:1} == "-" ]; then
          echo "-r used without repository name argument!"
          exit 1
      fi
      REPOSITORY=$arg
      ;;        
      -d|--domain)
      if [ ${arg:0:1} == "-" ]; then
          echo "-d used without domain name argument!"
          exit 1
      fi
      DOMAIN=$arg
      ;;
      -m|--email)
      if [ ${arg:0:1} == "-" ]; then
          echo "-m used without e-mail address argument!"
          exit 1
      fi
      EMAIL=$arg
      ;;
  esac
}

ArgumentsValidate() {
  if [ -z "$REPOSITORY" ]; then
    ShowSintax
  fi

  if [ -z "$DOMAIN" ]; then
    ShowSintax
  fi

  if [ -z "$EMAIL" ]; then
    ShowSintax
  fi

  if [ -z "$DNSPROVIDER" ]; then
    ShowSintax
  fi

  if [ $HOST_SCRIPTS -eq 0 ]; then
    HOST_DNSSERVICE_PROVIDER_SCRIPT_PATH="$HOST_DNSSERVICE_SCRIPT_PATH/$DNSPROVIDER"
    if [ ! -d $HOST_DNSSERVICE_PROVIDER_SCRIPT_PATH ]; then
      echo "Invalid dns service provider $DNSPROVIDER path. Choose one below or create one:"
      ls -d $HOST_DNSSERVICE_SCRIPT_PATH/*/ | cut -f2 -d'/'
      exit 1
    fi
  else
    if [ ! -d $HOST_DNSSERVICE_PROVIDER_SCRIPT_PATH ]; then
      echo "Invalid dns service provider $HOST_DNSSERVICE_PROVIDER_SCRIPT_PATH path."
      exit 1
    fi
  fi

  # api.key
  if [ ! "$(docker secret ls -f name=api.key --format "{{.Name}}")" ==  "api.key" ]; then
    echo -e "\nAdd the DNS service api.key secret.\n"
    ShowSintax
  fi

  # Test or dry
  if [ ! -z "$CERTOBOT_DRY" ] && [ ! -z "$CERTOBOT_TEST" ]; then
    echo -e "\nCan't run test and dry options together.\n"
    ShowSintax
  fi
  # Unprivileged
  if [ ! -z "$UNPRIVILEGED" ]; then
    if [ -z "$HOST_CERT_PATH" ]; then
      echo -e "\nProvide a path to unprivileged user create certificates using -cp option.\n"
      ShowSintax
    fi
    UNPRIVILEGED="-u $UNPRIVILEGED"
  fi

  # Cert path
  if [ ! -z "$HOST_CERT_PATH" ]; then
    CERT_PATH=$HOST_CERT_PATH
  else
    if [ ! -d "$CERT_PATH" ]; then
        if [ "$(id -u)" -ne 0 ]; then
          echo -e "\nPlease run as root or create /etc/letsencrypt before running."
          exit 1
        fi
    fi    
  fi
  
  CONTAINER_DNSSERVICE_PROVIDER_SCRIPT_PATH="$CONTAINER_DNSSERVICE_SCRIPT_PATH/$DNSPROVIDER"
  CERTBOT_CONFIG_PATH="--config-dir $CONTAINER_CERT_PATH --logs-dir $CONTAINER_CERT_PATH/logs --work-dir $CONTAINER_CERT_PATH/work"
}

Arguments(){

  # Parse Args
  while [[ $# -gt 0 ]]; do
    key="$1"
    arg="$2"
    ArgumentsParse
    shift
  done

  # Validate args
  ArgumentsValidate

}

CertPath(){
  # create path on server
  if [ ! -d "$CERT_PATH" ];then
    echo -e "\nCreating certificates path $CERT_PATH...\n"
    mkdir -p -m 0700 $CERT_PATH
  fi
  if [ ! -z $HOST_CERT_PATH ]; then
    echo -e "\nUpdating cert path permissions...\n"
    chmod -R 755 $HOST_CERT_PATH
  fi  
}

CertbotOptions(){
  # to work without prefix also
  local DOMAIN_WILDCARD="$DOMAIN,*.$DOMAIN"
  CERTBOT_MODE="$CERTBOT_MODE$VERBOSE$CERTOBOT_DRY$CERTOBOT_TEST"

  CERTBOT_OPTIONS="$CERTBOT_MODE \\
        $CERTBOT_CONFIG_PATH \\
        --preferred-challenges dns \\
        --manual-auth-hook $CONTAINER_DNSSERVICE_PROVIDER_SCRIPT_PATH/dnsauth.sh \\
        --manual-cleanup-hook $CONTAINER_DNSSERVICE_PROVIDER_SCRIPT_PATH/dnsclean.sh \\
        --agree-tos --eff-email --renew-by-default --renew-with-new-domains \\
        --cert-name $DOMAIN \\
        -d $DOMAIN_WILDCARD \\
        -m $EMAIL"
}

ContainerServiceRun() {
  echo -e "\nRunning...\n"
  CertbotOptions

  # if using dns scripts in host, set mount in container
  if [ $HOST_SCRIPTS -eq 1 ]; then
    # not necessary but good to organize ideas, performance not an issue.
    HOST_DNSSERVICE_SCRIPT_PATH="$HOST_WORK_PATH/$DNSPROVIDERPATH"

    HOST_SCRIPT="--mount type=bind,source=$HOST_DNSSERVICE_PROVIDER_SCRIPT_PATH,destination=$CONTAINER_DNSSERVICE_PROVIDER_SCRIPT_PATH"
  fi
  local EX=$(echo -e "docker service create --name certgen \\
        -t $CONTAINER_DETACH \\
        -e CERT_PATH=$CONTAINER_CERT_PATH \\
        --mount type=bind,source=$CERT_PATH,destination=$CONTAINER_CERT_PATH \\
        $HOST_SCRIPT \\
        --secret api.key \\
        --restart-delay $delay \\
        --restart-max-attempts 0 \\
        --restart-condition none \\
        $UNPRIVILEGED --replicas 1 \\
        $REPOSITORY/certgen:$tagversion \\
        certonly \\
        $CERTBOT_OPTIONS")
  if [ ! -z "$VERBOSE" ]; then
      echo "$EX"
  fi
  eval "$EX"
}

ContainerWaitFinish() {
  sleep 2
  local CONTAINER=$(docker ps -a --filter name=certgen --format "{{.ID}}")
  if [ -z $CONTAINER ]; then
    echo -e "\nTheres no container service running, leaving.\n"
    CONTAINER_RUINNING="false"
    exit 0
  fi
  echo -e "\nChecking container state...\n"
  CONTAINER_RUINNING=$(docker container inspect -f '{{.State.Running}}' $CONTAINER)

  # alternate check in State json object: state="$(docker container inspect -f '{{.State.Status}}' $(docker ps -a --filter name=certgen --format "{{.ID}}"))"
  while [ "$CONTAINER_RUINNING" == "true" ]
  do
    echo -e "\nWaiting for Certbot container to finish its job...\n"
    sleep 5
    CONTAINER_RUINNING=$(docker container inspect -f '{{.State.Running}}' $CONTAINER)
  done
}

ContainerLogs(){
  if [ ! -z "$VERBOSE" ]; then
    docker service logs -t --raw certgen
  fi
}

ContainerRemove() {
  local CONTAINER=$(docker ps -a --filter name=certgen --format "{{.ID}}")
  if [ ! -z $CONTAINER ]; then  
    echo -e "Removing the service container...\n"
    docker service rm certgen
    echo -e "\n"
  fi
}

PEMBuild() {
  local PEMFILENAME="$DOMAIN.pem"
  local LETSENCRYPTLIVEPATH="$CERT_PATH/live/$DOMAIN"

  if [ ! -d $LETSENCRYPTLIVEPATH ]; then
    echo -e "\nNo live certificate path created at $LETSENCRYPTLIVEPATH.\n"
    exit 0
  fi

  echo -e "$(cat $LETSENCRYPTLIVEPATH/cert.pem)" > $CERT_PATH/"${PEMFILENAME}_cert"
  echo -e "$(cat $LETSENCRYPTLIVEPATH/privkey.pem)" > $CERT_PATH/"${PEMFILENAME}_privkey"
  echo -e "$(cat $LETSENCRYPTLIVEPATH/fullchain.pem)" > $CERT_PATH/"${PEMFILENAME}fullchain"

  if [ $ONEFILEPEM -eq 1 ]; then
      echo -e "$(cat $LETSENCRYPTLIVEPATH/cert.pem $LETSENCRYPTLIVEPATH/fullchain.pem $LETSENCRYPTLIVEPATH/privkey.pem)" >  $CERT_PATH/$PEMFILENAME
      openssl x509 -in $CERT_PATH/$PEMFILENAME -noout -text
      echo -e "\nCertificate $PEMFILENAME at $CERT_PATH created.\n"
    else
      openssl x509 -in $CERT_PATH/"${PEMFILENAME}_cert" -noout -text
      echo -e "\nCertificate ${PEMFILENAME}_cert at $CERT_PATH created.\n"
  fi
}

# Credits: https://unix.stackexchange.com/questions/13591/better-way-of-accepting-variations-of-yes-from-a-shell-prompt/14444#14444
Confirm() {
  echo -e "\n"
  read -sn 1 -p "$* [Y/N]? "; [[ ${REPLY:0:1} = [Yy] ]];
}

Cleanup() {

  # if not verbose mode leave logs of execution
  if [ -z "$VERBOSE" ]; then
      exit 1
  fi

  echo -e "\nCleaning up...\n"

  if Confirm "Remove container"; then
    ContainerRemove
  fi

  if Confirm "Remove DNS service api.key secret"; then
    docker secret rm api.key
  fi

  if Confirm "Remove certificates path"; then
    if [ ! -z $CERT_PATH ]; then
      rm -Rf $CERT_PATH/
    fi
  fi

  echo -e "\n"
}

Main() {
  ShowLogo

  # No args
  if [ $# -eq 0 ]; then
    ShowSintax
  fi

  Arguments "${@:-}" # parse and check arguments

  CertPath # build cert path if necessary

  ContainerRemove # just to be sure theres nothing already running

  ContainerServiceRun # Run the container

  ContainerWaitFinish # Monitor execution

  PEMBuild # Build the PEM

  Cleanup
}

# Start here
Main "${@:-}"