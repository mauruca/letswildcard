```
    __          __           _       __ _  __                        __
   / /   ___   / /_ _____   | |     / /(_)/ /_____ ____ _ _____ ____/ /
  / /   / _ \ / __// ___/   | | /| / // // // ___// __ `// ___// __  / 
 / /___/  __// /_ (__  )    | |/ |/ // // // /__ / /_/ // /   / /_/ /  
/_____/\___/ \__//____/     |__/|__//_//_/ \___/ \__,_//_/    \__,_/    
```
----

Conternized wildcard certificates using LetsEncrypt DNS auth.

## How
It uses the manual certification process adding 2 custom scripts to add and remove a letsencrypt auth TXT record in DNS. Your DNS provider must allow these changes through api.

[Certbot manual docs](https://certbot.eff.org/docs/using.html#manual)

## Key Features
* **One certificate for each domain.**
* Container generated certificates, no need to install certbot.
* Only DNS wildcard certificates. Why not? It creates a SAN certificate for ***mydomain.tld and *.mydomain.tld**.
* Certificates can be added to docker as service secret or to a path on server.
* Can run using unprivileged users.

## Building the container
Run the build command passing the version as argument. It will create a container named `<domain>/certgen:<version>`. The current version tag used in version file.

```bash
$ 1.build.sh <reponame> <dnsservicename>
```
If you need to push the files to docker hub run the command below. Be sure that you signed in at docker hub with your domain as user, and already logged in you local docker engine (docker login).
```bash
$ 2.push.sh <reponame>
```

## Creating certificates

### Before Run Config
* Install docker
* Add a secret to docker named `api.key` to store your api key. Add the server ip to your api ip control.
* Run the command as root if you are using the default path `/etc/letsencrypt`.

### Options
* -v  | --verbose      => to return more information about the process.
* -t  | --test         => generates a certificate from letsencrypt stage server.
* -dr | --dryrun       => test in production server but do not generates a certificate.
* -cp | --certpath     => generated certificates base path.
* -u  | --unprivileged => run the service using a UID:GID instead of root. Must define a custom --certpath where unpriviledge user has permission to write.
* -of |--onefilepem    => also build one file pem that will contain letsencrypt public, fullchain and privkey.
* -hs |--hostscripts <scripts path> => use challenge scripts on host.

### Required
* -r  | --repository  <repository>      => docker hub repository name. ex: `mydomain`.
* -dp | --dnsprovider <dnsprovidername> => dns provider name. It must exists as a path inside dnsservice path.
* -d  | --domain      <domain>          => domain name. ex: `mydomain.tld`.
* -m  | --email       <email>           => domain admin email. ex: `admin@mydomain.tld`.

### Run
* Adding test certificate to secrets
```bash
3.run.sh -v -t -s -r mydomain -dp mydns -d mydomain.com -m admin@mydomain.com
```

* Adding test certificate to default /etc/letsencrypt path
```bash
3.run.sh -v -t -r mydomain -dp mydns -d mydomain.com -m admin@mydomain.com
```

* Adding test certificate to defined path
```bash
3.run.sh -v -t -cp $PWD/cert -r mydomain -dp mydns -d mydomain.com -m admin@mydomain.com
```

* Adding test certificate to defined path and unprivileged user
```bash
3.run.sh -v -t -u 1000:1000 -cp $PWD/cert -r mydomain -dp mydns -d mydomain.com -m admin@mydomain.com
```

* Dry run in production
```bash
3.run.sh -v -dr -cp $PWD/cert -r mydomain -dp mydns -d mydomain.com -m admin@mydomain.com
```

* Create certificates to default path
```bash
3.run.sh -r mydomain -dp mydns -d mydomain.com -m admin@mydomain.com
```

## Important notes
* Letsencrypt enforce some limits of use for production and test.
There is a Failed Validation limit of 5 failures per account, per hostname, per hour in production server, and 60 in test staging server.
Check [Letsencrypt limits](https://letsencrypt.org/docs/rate-limits/) for details.


## Verify DNS entries

If an error occour, please check DNS entries for certbot and delete them. For example:

| Type | Name | Data |
|:----:|:----:|:----:|
| TXT | _acme-challenge | "9BVT7VfIFf8Y-sTPxjNHBNVwzbLNHt-lYSOMnClTI5" |

## TODO
* Add scripts for more DNS APIs.
* Create a cleanup script to all certbot DNS entries in case of error.
* Allow service to continuous certificate update controled by docker.
* Support k8 secrets.

## Known bugs or issues
* It challenge takes at least 30s waiting for dns propagate changes. Certbot bot does 2 challenges one for mydomain.tld and one for *.mydomain.tld**, then it will take at least 60s to run.
* Already issued certs in test or in production mode, generates a false positive when checking container state.

## Similar or better projects
* https://github.com/dehydrated-io/dehydrated - More generalist implementation to certbot.
* https://github.com/AnalogJ/lexicon - Standarlized DNS manipulation using APIs