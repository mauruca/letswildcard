#!/bin/sh

# modified from https://certbot.eff.org/docs/using.html#manual example

# Get your API key
API_KEY=$(cat /run/secrets/api.key)

API_SERVER="https://api.vultr.com/v2/domains"

URL="$API_SERVER/$CERTBOT_DOMAIN/records"

# vultr has one dns for each domain id removes domain name from name variable.
NAME="_acme-challenge.$CERTBOT_DOMAIN"

DATA="{\"type\":\"TXT\",\"name\":\"$NAME\",\"data\":\"$CERTBOT_VALIDATION\",\"ttl\":120}"

# improved md5 due to cetbot challenges for domain.tld and *.domain.tld. Fixes a bug of not removing all dns records
MD5=$(echo "$NAME$CERTBOT_VALIDATION" | md5sum | cut -d' ' -f1)

# Create TXT record
REQ="curl -s -X POST \"$URL\" \
     -H \"Authorization: Bearer $API_KEY\" \
     -H \"Content-Type: application/json\" \
     --data '$DATA'"

RESPONSE=$(eval $REQ)

RECORD_ID=$( echo $RESPONSE | python -c "import sys,json;print(json.load(sys.stdin)['record']['id'])")

echo $RECORD_ID > $CERT_PATH/$MD5

echo -e "*****************************\n"
echo -e "*****************************\n"
echo -e "$CERTBOT_DOMAIN\n"
echo -e "$MD5\n"
echo -e "$NAME\n"
echo -e "$DATA\n"
echo -e "$RESPONSE\n"
echo -e "*****************************\n"
echo -e "*****************************\n"

# Sleep to make sure the change has time to propagate over to DNS
sleep 30