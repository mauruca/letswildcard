#!/bin/sh

# modified from https://certbot.eff.org/docs/using.html#manual example

# Get your API key
API_KEY=$(cat /run/secrets/api.key)

API_SERVER="https://api.vultr.com/v2/domains"

URL="$API_SERVER/$CERTBOT_DOMAIN/records"

NAME="_acme-challenge.$CERTBOT_DOMAIN"

# improved md5 due to cetbot challenges for domain.tld and *.domain.tld. Fixes a bug of not removing all dns records
MD5=$(echo "$NAME$CERTBOT_VALIDATION" | md5sum | cut -d' ' -f1)

#check for file created on auth
if [ -f $CERT_PATH/$MD5 ]; then
        
        RECORD_ID=$(cat $CERT_PATH/$MD5)
        
        rm -f $CERT_PATH/$MD5

        # Remove the challenge TXT record from the zone
        if [ -n "${RECORD_ID}" ]; then
                REQ="curl -s -X DELETE \"$URL/$RECORD_ID\" \
                        -H \"Authorization: Bearer $API_KEY\" \
                        -H \"Content-Type: application/json\""

                RESPONSE=$(eval $REQ)
        fi
        echo -e "*****************************\n"
        echo -e "*****************************\n"
        echo -e "$CERTBOT_DOMAIN\n"
        echo -e "$MD5\n"
        echo -e "$RESPONSE\n"
        echo -e "*****************************\n"
        echo -e "*****************************\n"
else
        echo -e "No file found."
fi



